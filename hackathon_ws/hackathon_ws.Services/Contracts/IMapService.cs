﻿using hackathon_ws.Services.Model;
using System.Threading.Tasks;

namespace hackathon_ws.Services.Contracts
{
    public interface IMapService
    {
        Task<Feature[]> GetAsync(string prop);
    }
}
