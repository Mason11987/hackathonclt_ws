﻿namespace hackathon_ws.Services.Model
{
    public class Feature
    {
        public string type { get; set; }
        public Props properties { get; set; }
        public Geometry geometry { get; set; }

    }
}