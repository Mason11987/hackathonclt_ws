﻿namespace hackathon_ws.Services.Model
{
    public class Geometry
    {
        public string type { get; set; }
        public Coordinate[] coordinates { get; set; }
    }
}