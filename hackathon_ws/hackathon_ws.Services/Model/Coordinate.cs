﻿namespace hackathon_ws.Services.Model
{
    public class Coordinate
    {
        public float lat { get; set; }
        public float lng { get; set; }
    }
}