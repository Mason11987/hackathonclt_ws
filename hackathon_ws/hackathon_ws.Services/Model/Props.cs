﻿namespace hackathon_ws.Services.Model
{
    public class Props
    {
        public string id { get; set; }
        public string name { get; set; }
        public string value { get; set; }
        public string color { get; set; }
    }
}