﻿using hackathon_ws.Services.Contracts;
using hackathon_ws.Services.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hackathon_ws.Services
{
    public class MapService : IMapService
    {
        public async Task<Feature[]> GetAsync(string prop)
        {
            if (string.IsNullOrEmpty(prop))
                prop = "Low_Cost_Healthcare_Proximity_2016";
            var path = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
            var file = "qol-npa-mapping.geojson";
            var filepath = Path.Combine(path, file);
            var jsonText = File.ReadAllText(filepath);

            var map = JsonConvert.DeserializeObject<Map>(jsonText);

            AddValues(map, prop);

            AddNames(map);

            return map.features;
        }

        private void AddNames(Map map)
        {
            var path = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);

            var csvfile = "qol-npa-mapping.csv";
            var csvPath = Path.Combine(path, csvfile);
            var csvText = File.ReadAllLines(csvPath);

            var dict = csvText.Skip(1).Select(x => x.Split(',')).ToDictionary(key => key[0], val => val[1]);

            foreach (var feature in map.features)
            {
                if (dict.ContainsKey(feature.properties.id))
                {
                    feature.properties.name = dict[feature.properties.id];
                }
            }
        }

        private void AddValues(Map map, string prop)
        {
            var path = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);

            var csvfile = "qol-health.csv";
            var csvPath = Path.Combine(path, csvfile);
            var csvText = File.ReadAllLines(csvPath);

            var index = csvText.First().Split(',').ToList().IndexOf(prop);
            var dict = csvText.Skip(1).Select(x => x.Split(',')).ToDictionary(key => key[0], val => val[index]);
            var max = (float)dict.Values.Select(x => string.IsNullOrEmpty(x) ? int.MinValue : Convert.ToDouble(x)).Max();
            var min = (float)dict.Values.Select(x => string.IsNullOrEmpty(x) ? int.MaxValue : Convert.ToDouble(x)).Min();

            foreach (var feature in map.features)
            {
                if (dict.ContainsKey(feature.properties.id))
                {
                    var val = dict[feature.properties.id];
                    feature.properties.value = val;
                    feature.properties.color = GetColor(min, val, max, prop.Contains("Births_to_Adolescents"));
                }
                else
                    feature.properties.color = "#FFFFFF";
            }
        }

        private string GetColor(float min, string val, float max, bool invert)
        {
            Color color = Color.White;
            if (string.IsNullOrEmpty(val))
                return HexConverter(color);
            else
            {
                var iVal = (float)Convert.ToDouble(val);
                var ratio = (iVal - min) / (max - min);
                if (invert)
                    ratio = 1 - ratio;
                if (ratio < .5)
                    color = Color.FromArgb(255, 255, (int)(255 * (ratio * 2)), 0);
                else
                    color = Color.FromArgb(255, (int)(255 * (1- ((ratio-.5) * 2))), 255, 0);

                return HexConverter(color);
            }
        }

        private string HexConverter(Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }
    }
}
