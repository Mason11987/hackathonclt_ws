﻿using AutoMapper;
using hackathon_ws.API.DataContracts.Requests;
using hackathon_ws.Services.Contracts;
using hackathon_ws.Services.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using S = hackathon_ws.Services.Model;

namespace hackathon_ws.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/users")]//required for default versioning
    [Route("api/v{version:apiVersion}/maps")]
    public class MapController : Controller
    {
        private readonly IMapService _service;
        private readonly IMapper _mapper;

        public MapController(IMapService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        #region GET

        public async Task<Feature[]> Get(string prop)
        {
            var data = await _service.GetAsync(prop);

            return data;
            //if (data != null)
            //    return _mapper.Map<User>(data);
            //else
            //    return null;
        }
        #endregion

    }
}
