﻿using AutoMapper;
using DC = hackathon_ws.API.DataContracts;
using S = hackathon_ws.Services.Model;

namespace hackathon_ws.IoC.Configuration.AutoMapper.Profiles
{
    public class APIMappingProfile : Profile
    {
        public APIMappingProfile()
        {
            CreateMap<DC.User, S.User>().ReverseMap();
            CreateMap<DC.Adress, S.Adress>().ReverseMap();
        }
    }
}
