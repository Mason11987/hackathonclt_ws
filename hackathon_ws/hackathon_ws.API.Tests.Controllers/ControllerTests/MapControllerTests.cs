﻿using AutoMapper;
using hackathon_ws.API.Controllers;
using hackathon_ws.Services.Contracts;
using hackathon_ws.Services.Model;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace hackathon_ws.API.Tests.Controllers.ControllerTests
{
    [TestClass]
    public class MapControllerTests : TestBase
    {
        //NOTE: should be replaced by an interface
        MapController _controller;

        public MapControllerTests() : base()
        {
            var serviceProvider = _services.BuildServiceProvider();
            var businessService = serviceProvider.GetRequiredService<IMapService>();
            var mapper = serviceProvider.GetRequiredService<IMapper>();

            _controller = new MapController(businessService, mapper);
        }


        [TestMethod]
        public void TestGetMap()
        {



            var result = _controller.Get("").Result;
        }

    }
}
